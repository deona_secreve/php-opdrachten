<?php

/**
 * @file
 * Show the word cat 3 times.
 *
 * @author Deona Secreve
 *
 * @version 1.0
 */

$str = "cat";

/**
 * Repeat the string 3 times.
 *
 * @param string $str
 *   String with the word cat in it.
 *
 * @return string
 *   Return the reaped string.
 */
function repeat_string($str) {
  $str = str_repeat($str, 3);
  return $str;
}

echo repeat_string($str);
