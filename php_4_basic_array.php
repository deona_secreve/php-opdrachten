<?php

/**
 * @file
 * Summing the array and return the integer.
 *
 * @author Deona Secreve
 *
 * @version 1.0
 */

$numbers = [5, 15, 25, -10];

/**
 * Count the numbers in the array with array sum.
 *
 * @param string $numbers
 *   Array with the numbers in it.
 *
 * @return string
 *   return the summed array
 */
function number_array($numbers) {

  return "Sum: " . array_sum($numbers);
}

echo number_array($numbers);
