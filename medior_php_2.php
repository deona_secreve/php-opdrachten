<?php

/**
 * @file
 * Sum of all numbers odd or even.
 *
 * @author Deona Secreve
 *
 * @version 1.0
 */

$numbers = [15, 15, 14, 20, -20];

/**
 * Sum the numbers in the array and check if the result is even or odd.
 *
 * @param array $numbers
 *   Array that contains the integers.
 *
 * @return string
 *   return if the sum of all numbers is odd or even.
 */
function array_odd_even(array $numbers) {
  $evenOdd = array_sum($numbers);
  if ($evenOdd % 2 == 0) {
    return 'even';
  }
  return 'odd';
}

echo array_odd_even($numbers);
