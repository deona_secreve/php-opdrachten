<?php

/**
 * @file
 * Escape all spaces from the string.
 *
 * @author Deona Secreve
 *
 * @version 1.0
 */

$str = "hello world and sphynx cats";

/**
 * Remove the string spaces, skip the spaces and replace them with no space.
 *
 * @param string $str
 *   String with the text with spaces.
 *
 * @return null|string|string[]
 *   Returns the string without white spaces.
 */
function no_spaces($str) {
  $str = preg_replace('/\s+/', '', $str);
  return $str;
}

echo no_spaces($str);
