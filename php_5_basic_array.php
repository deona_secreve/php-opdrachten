<?php

/**
 * @file
 * Search in the array for the word needle and return it with the position.
 *
 * @author Deona Secreve
 *
 * @version 1.0
 */

$haystack = [
  'hay',
  'junk',
  'poezen',
  'sphynx',
  'needle',
  'milk',
  'cornflakes',
];

/**
 * Search for the word needle.
 *
 * @param string $haystack
 *   In the string $hst is a array that include the stuff.
 *
 * @return string
 *   Return needle and show the position.
 */
function find_needle($haystack) {

  return 'found the needle at position ' . array_search('needle', $haystack);
}

echo find_needle($haystack);
