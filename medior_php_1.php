<?php

/**
 * @file
 * Calculates the range, average en median with the given data string.
 *
 * @author Deona Secreve
 *
 * @version 1.0
 */

/**
 * Calculates the range, average and median.
 *
 * @return string
 *   return the array that is summed.
 */
function athletic_computer_nerd() {
  $data = "01|15|59, 01|47|6, 2|17|20, 3|32|34";
  if ($data == "") {
    return "";
  }
  $athleticsArr = explode(", ", $data);
  $athleticsArr = array_map(function ($value) {
    $var = explode("|", $value);
    return ($var[0] * 3600 + $var[1] * 60 + $var[2]);
  }, $athleticsArr);

  sort($athleticsArr);
  $range = max($athleticsArr) - min($athleticsArr);
  $average = array_sum($athleticsArr) / count($athleticsArr);
  $median = (count($athleticsArr) % 2 == 0) ? (($athleticsArr[(count($athleticsArr) / 2) - 1] + $athleticsArr[(count($athleticsArr) / 2)]) / 2) : ($athleticsArr[floor(count($athleticsArr) / 2)]);
  return "Range: " . to_seconds($range) . " Average: " . to_seconds($average) . " Median: " . to_seconds($median);
}

/**
 * Calculates hours in seconds and minutes in seconds.
 *
 * @param string $seconds
 *   Hours and minutes are seconds.
 *
 * @return string
 *   Return the hours, minutes and seconds in the given format.
 */
function to_seconds($seconds) {
  $hours = floor($seconds / 3600);
  $minutes = floor(($seconds - $hours * 3600) / 60);
  $seconds = $seconds - $hours * 3600 - $minutes * 60;
  return sprintf("%02d|%02d|%02d", $hours, $minutes, $seconds);
}

echo athletic_computer_nerd();
