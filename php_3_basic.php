<?php

/**
 * @file
 * Show if a number is odd or even between 0 and 10.
 *
 * @author Deona Secreve
 *
 * @version 1.0
 */

/**
 * Look in the number 0 to 10 if its a odd or even number.
 */
function string_odd_even($num) {
  $evenOdd = ($num % 2 == 0) ? "Even " : "Odd ";
  return $num . ":" . $evenOdd;
}

for ($num = 0; $num < 10; $num++) {
  echo string_odd_even($num);
}
